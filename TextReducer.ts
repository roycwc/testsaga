import { Reducer } from 'redux';
import { getType } from 'typesafe-actions';
import { RootActions } from './reducers';
import * as Actions from './actions';

export interface TextState {
  greet: string;
}

const iniUiState = {
  greet: 'Hello World',
};

export type TextReducer = Reducer<TextState, RootActions>;

export const textReducer: TextReducer = (state = iniUiState, action) => {
  if (action.type === getType(Actions.setGreet)) {
    return {
      ...state,
      greet: (action as Actions.SetGreetActionType).payload,
    };
  }
  return state;
};
