module.exports = {
  devtool: "inline-source-map",
  module: {
    rules: [{
      loader: 'ts-loader',
      test: /\.tsx?$/
    }]
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  entry: './index.tsx',
  output: {
    filename:  'bundle.js',
    path: __dirname+'/dist'
  },
  mode: 'development'
}