import { createAction } from 'typesafe-actions';

export const CHANGE_GREET = 'CHANGE_GREET';

export const setGreet = createAction('SET_GREET', (resolve) => {
  return (greet: string) => resolve(greet);
});

export const changeGreet = createAction(CHANGE_GREET, (resolve) => {
  return () => resolve();
});

export type SetGreetActionType = ReturnType<typeof setGreet>;
export type ChangeGreetActionType = ReturnType<typeof changeGreet>;
