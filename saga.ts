import { setGreet, CHANGE_GREET } from './actions';
import { put, all, take, call } from 'redux-saga/effects';

function* watchChangeGreet() {
  while (true) {
    yield take(CHANGE_GREET);
    yield put(setGreet('Bye'));
    yield take(CHANGE_GREET);
    yield put(setGreet('Still Here?'));
  }
}

export const rootSaga = function* () {
  yield all([
    call(watchChangeGreet),
  ]);
};
