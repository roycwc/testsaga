# Minimal demo on TS, React, Redux, Redux-Saga
JSX Action Dispatch > Saga Flow Control > Reducers change State > React re-render > JSX Action Dispatch > ...
All in Typescript

# How to Run 
```
npm install
npm run dev
npm run serve // in another terminal
```

# What's so special
- `redux-saga` control the sequence of button states
- `Typescript` make Redux Actions so complex, that even need a seperate `typesafe-actions` library to handle all boilerplates
- `mapDispatchToProps`, `mapStateToProps`, `combineReducers` typings works, and start appreciate it everyday 
- Never need `babel-*` again, use `ts-loader` instead
- `tslint` works great, as well as `autoformat` and `autolinting`
- `webpack` will watch all `tsx` `ts` file changes and bundle a single js file in `dist`, and trigger `browser-sync` to reload

# VSCODE settings
For auto-formatting and ts-lint-ing:

```json
{
    "editor.tabSize": 2,
    "tslint.autoFixOnSave": true,
    "editor.formatOnSave": true,
}
```

