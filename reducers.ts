import { combineReducers, ReducersMapObject } from 'redux';
import { textReducer, TextReducer, TextState } from './TextReducer';
import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type RootActions = ActionType<typeof actions>;

export interface RootState {
  texts: TextState;
}

const rootReducers = {
  texts: textReducer,
};

export const reducers = combineReducers<RootState>(rootReducers);
