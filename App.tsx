import * as React from 'react';
import { connect } from 'react-redux';
import { RootState } from './reducers';
import { TextState } from './TextReducer';
import { Dispatch } from 'redux';
import * as Actions from './actions';
import { ActionType } from 'typesafe-actions';
export type RootActions = ActionType<typeof Actions>;

interface DispatchProp {
  changeGreet: () => Actions.ChangeGreetActionType;
}

const mapDispatchToProps = (dispatch: Dispatch<RootActions>): DispatchProp => ({
  changeGreet: () => dispatch(Actions.changeGreet()),
});

const mapStateToProps = (state: RootState): TextState => state.texts;

const app = (props: TextState & DispatchProp) => (
  <button onClick={props.changeGreet} >{props.greet}</button>
);

export default connect(mapStateToProps, mapDispatchToProps)(app);
